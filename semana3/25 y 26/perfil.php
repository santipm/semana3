<?php

//Cargar librerias
require_once('header.php');
require_once('menus.php');
require_once('users.php');

// Cargar Cabecera
get_header();

?>
<body>
		<!-- start: Header -->
		<?php 
			get_menu_top();
		?>
		<!-- start: Header -->
	
		<div class="container-fluid-full">
			<div class="row-fluid">
				
				<!-- start: Main Menu -->
				<?php get_main_menu(); ?>
				<!-- end: Main Menu -->
			
				<noscript>
					<div class="alert alert-block span10">
						<h4 class="alert-heading">Warning!</h4>
						<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
					</div>
				</noscript>
			
				<!-- start: Content -->
				<div id="content" class="span10">
					<div class="span12">
					<?php
						$breadcrumbs = array('Panel de control' => 'index.php', 'Perfil' =>'perfil.php');
						get_breadcrumbs($breadcrumbs); 
					?>
					</div>
					<div class="span10">
					 <!--Perfil -->
						<?php

							if(isset($_GET['id'])){
								$id = $_GET['id'];
								echo "id: $id </br>";
								$bandera = FALSE;
								
								foreach($usuarios as $value):
									//echo "valor:". $value['id']. '</br>';
									//echo 'id:'.$id;
									echo gettype($value['id']). '</br>';
									echo gettype($id). '</br>';
									if($value['id'] === (int)$id){
										echo 'entra';
										$bandera = TRUE;	
										break;
									}
								endforeach;
								
								if($bandera){
									echo 'Existe el usuario';
								}else{
									echo 'No existe el usuario';
								}
							}
							
							
						?>
					</div>
				</div><!--/.fluid-container-->
	
			<!-- end: Content -->
		</div><!--/#content.span10-->
		
	
	<div class="clearfix"></div>
	
	<footer>

		<p>
			<span style="text-align:left;float:left">&copy; 2015 <a href="http://jiji262.github.io/Bootstrap_Metro_Dashboard/" alt="Bootstrap_Metro_Dashboard">Curso Wordpress a tu medida</a></span>
			
		</p>

	</footer>
	
	<!-- start: JavaScript-->

	<?php 
		require('libreria_js.php');
	?>
	
</body>
</html>
