<?php

//Cargar librerias
require_once('header.php');
require_once('menus.php');
require_once('users.php');

// Cargar Cabecera
get_header();

$validarCampos = FALSE;

if ($_SERVER['REQUEST_METHOD']=='POST'){
	
	
	if(!empty($_POST['nombre']) and !empty($_POST['apellidos']) and $_FILES['fichero']['error']!==4 ){
		$nombre = $_POST['nombre'];
		$apellidos = $_POST['apellidos'];
		$ficheroTMP = $_FILES['fichero']["tmp_name"];
		
		print_r($_POST);
		print_r($_FILES);
		
		$movido = move_uploaded_file($ficheroTMP, "img/$nombre-$apellidos.jpg");
		
		if(!$movido){echo "OK";}
	}else{
		$validarCampos = TRUE;
	}

	
	//Coger datos y enviar email
	
}
?>
<body>
		<!-- start: Header -->
		<?php 
			get_menu_top();
		?>
		<!-- start: Header -->
	
		<div class="container-fluid-full">
			<div class="row-fluid">
				
				<!-- start: Main Menu -->
				<?php get_main_menu(); ?>
				<!-- end: Main Menu -->
			
				<noscript>
					<div class="alert alert-block span10">
						<h4 class="alert-heading">Warning!</h4>
						<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
					</div>
				</noscript>
			
				<!-- start: Content -->
				<div id="content" class="span10">
					<div class="span12">
					<?php
						$breadcrumbs = array('Panel de control' => 'index.php', 'Perfil' =>'perfil.php');
						get_breadcrumbs($breadcrumbs); 
					?>
					</div>
					<div class="span10">
						
					<!-- Validación de campos -->
						<?php if($validarCampos){ ?>
								<div class="alert alert-error">
									<button type="button" class="close" data-dismiss="alert">×</button>
									<strong>Por favor,</strong> Debe de rellenar todos los campos 
								</div>
						<?php } ?>
						
					 <!--Perfil -->
						<form action=<?php echo $_SERVER['PHP_SELF']; ?> method="post" enctype="multipart/form-data">
							<div class="control-group">
							  <label class="control-label" for="typeahead">Nombre </label>
							  <div class="controls">
								<input type="text" class="span6 typeahead" name="nombre">
							  </div>
							</div>
							<div class="control-group">
							  <label class="control-label" for="typeahead">Apellidos </label>
							  <div class="controls">
								<input type="text" class="span6 typeahead" name="apellidos">
							  </div>
							</div>
							<div class="control-group">
							  <label class="control-label" for="typeahead">Curriculum </label>
							  <div class="controls">
								<input type="file" class="span6 typeahead" name="fichero">
							  </div>
							</div>
							<input type="submit" class="btn btn-primary" value="Enviar">
						</form>
					</div>
				</div><!--/.fluid-container-->
	
			<!-- end: Content -->
		</div><!--/#content.span10-->
		
	
	<div class="clearfix"></div>
	
	<footer>

		<p>
			<span style="text-align:left;float:left">&copy; 2015 <a href="http://jiji262.github.io/Bootstrap_Metro_Dashboard/" alt="Bootstrap_Metro_Dashboard">Curso Wordpress a tu medida</a></span>
			
		</p>

	</footer>
	
	<!-- start: JavaScript-->

	<?php 
		require('libreria_js.php');
	?>
	
</body>
</html>
