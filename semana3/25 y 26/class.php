<?php
class Persona{
	
	//Atributos
	public $edad;
	public $nombre;
	protected $colorPelo;
	private $fuerzas;
	
	//Conctructor
	function __construct($edad,$nombre,$colorPelo,$fuerzas=100){
		$this->edad = $edad;
		$this->nombre = $nombre;
		$this->colorPelo = $colorPelo;
		$this->fuerzas = $fuerzas;
		echo "Se ha creado el objeto";
	}
	
	
	//Métodos de visualización
	function getEdad(){
		return $this->edad;
	}
	
	//Métodos de modificación
	function setEdad($edad){
		echo "edad : $edad";
		$this->edad = $edad;
		echo "Entra";
	}
	
	function addFuerza($fuerzas){
		$this->fuerzas += $fuerzas;
	} 
	
	function getFuerza(){
		return $this->fuerzas;
	}
}


$Usuario1 = new Persona(10,"Santiago","Castaño");

$Usuario2 =  new Persona(20,"Santo","Castaño");

echo $Usuario1->getEdad();
echo $Usuario2->getEdad();

$Usuario1->addFuerza(50);
echo $Usuario1->getFuerza();
echo '</br>';
echo $Usuario2->getFuerza();
//$usuario1->setEdad(20);
//echo $usuario1->getEdad();
?>
