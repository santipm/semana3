<?php
//Cargar librerias
require_once('conexion.php');
require_once('header.php');
require_once('menus.php');
require_once('users.php');

//comprobamos si recibidos datos por POST
if(isset($_POST['accion'])){
	if($_POST['accion'] == 'editar'){
    	actualizar($_POST['id'],$_POST['nombre']);
		header("Location: index.php?modcorrect=1");
	}
}


// Cargar Cabecera
get_header('index');

?>
<body>
		<!-- start: Header -->
		<?php 
			get_menu_top();
		?>
		<!-- start: Header -->
	
		<div class="container-fluid-full">
			<div class="row-fluid">
				
				<!-- start: Main Menu -->
				<?php get_main_menu(); ?>
				<!-- end: Main Menu -->
			
				<noscript>
					<div class="alert alert-block span10">
						<h4 class="alert-heading">Warning!</h4>
						<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
					</div>
				</noscript>
			
				<!-- start: Content -->
				<div id="content" class="span10">
					<div class="span12">
					<?php
						$breadcrumbs = array('Panel de control' => 'index.php', 'Otro' =>'perfil.php');
						get_breadcrumbs($breadcrumbs); 
					?>
					</div>
					<div class="span10">
						
					 	<link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" /> 

						<!-- Inline CSS based on choices in "Settings" tab -->
						<style>.bootstrap-iso .formden_header h2, .bootstrap-iso .formden_header p, .bootstrap-iso form{font-family: Arial, Helvetica, sans-serif; color: black}.bootstrap-iso form button, .bootstrap-iso form button:hover{color: white !important;} .asteriskField{color: red;}</style>
						
						<!-- HTML Form (wrapped in a .bootstrap-iso div) -->
						<div class="bootstrap-iso">
						 <div class="container-fluid">
						  <div class="row">
						   <div class="col-md-6 col-sm-6 col-xs-12">
						    <form method="post">
						     <div class="form-group ">
						     <input type="hidden" name="accion" value="editar">
						     <input type="hidden" name="id" value="<?echo $_GET['id'];?>">
						      <label class="control-label " for="nombre">
						       Nombre
						      </label>
						      <input class="form-control" id="nombre" name="nombre" type="text"/>
						     </div>
						     <div class="form-group">
						      <div>
						       <button class="btn btn-primary " name="submit" type="submit">
						        Enviar
						       </button>
						      </div>
						     </div>
						    </form>
						   </div>
						  </div>
						 </div>
						</div>
					</div>
				</div>
			</div><!--/.fluid-container-->
	
			<!-- end: Content -->
		</div><!--/#content.span10-->
		
	
	<div class="clearfix"></div>
	
	<footer>

		<p>
			<span style="text-align:left;float:left">&copy; 2015 <a href="http://jiji262.github.io/Bootstrap_Metro_Dashboard/" alt="Bootstrap_Metro_Dashboard">Curso Wordpress a tu medida</a></span>
			
		</p>

	</footer>
	
	<!-- start: JavaScript-->

	<?php 
		require('libreria_js.php');
	?>
	
</body>
</html>
